## Samuel Markus Zammit - CSN 6.3A | IT Project GIT Upload 

Git URL - ```https://bitbucket.org/gtxsam/year3_project```

---
## Files Index
Click the links below to be taken to their respective commits

### 1. Section A - Version Control
[Literature Review](https://bitbucket.org/gtxsam/year3_project/commits/296db5076b1fdfd1abeca61a2f0183b2e2dddb9e)

[Methodology](https://bitbucket.org/gtxsam/year3_project/commits/5c92742daebd741af89e8ae823c13a34370cdd13)

[Interim Report](https://bitbucket.org/gtxsam/year3_project/commits/e52189421ded2c06945a9bda4fa59325af1dfff6)

[Results and Discussion](https://bitbucket.org/gtxsam/year3_project/commits/a015aa62d5f1505c6707f6047f26e12791b80c42)

[Conclusion, Introduction, Abstract](https://bitbucket.org/gtxsam/year3_project/commits/be1329e0cfc06ecc4e114a3a8d4b51c1730af044)

[Final Report](https://bitbucket.org/gtxsam/year3_project/commits/4b77ba300cd51ee79d3830c25c551ff8097a8975)

### 2. Section B - Interim Report
[Interim Report](https://bitbucket.org/gtxsam/year3_project/commits/e52189421ded2c06945a9bda4fa59325af1dfff6)

### 3. Section C - Final Report
[Final Report](https://bitbucket.org/gtxsam/year3_project/commits/4b77ba300cd51ee79d3830c25c551ff8097a8975)